# Introduction

You must write a program to create, draw and manipulate geometric
shapes on the screen. Part of the program is already available in the
source code.

The purpose of this TP is to:

1. Training on the concept of public heritage, and on the standard
   vector library
2. Use an existing C++ library to draw shapes.

Let's start by reading and understanding the existing code.

## Compilation

The program uses `cmake`. Follow the instructions in TP 1 to compile
and run programs. The repository contains two programs:

-  Example of using the `CImglibrary`
-  `shape_main`: the program to be modified.


## The CImg library

To visualize the shapes, we use the *CImg* library
<http://cimg.eu/>. The library consists of a single `CImg. h` file.
be included in our program. An example of a program is provided in the
src/main.cpp file, which generates the executable `exemple_main`.

The library allows to receive commands by the mouse and by keyboard:
the `main_disp.wait()` method waits for an *event*, a movement of the
mouse, a click of the mouse, or the press of a button. One can then
examine which event is about with methods like
`main_disp.is_key(cimg::keyA);`, or `main_disp.button()`, or
`main_disp.mouse_x()` and `main_disp.mouse_y()`.

Library documentation is provided in `CImg/CImg_reference.pdf`.


# Forms

The `src/shape_main.cpp` program allows you to create, select and
move geometric shapes in the program window.

Here is the diagram of the class hierarchy:

![img](shapes.png)

The shapes have a position `(x, y)` on the 2D plane. They can
be moved (change position) with the `move()` function and they
can be drawn with the `draw()` function.

The `is_inside(int x, int y)` function returns **true** if the point
`(x,y)` is within the form, and it is used to
select a shape with the mouse.

The `select()` function is used to select a shape. A form is initially
unselected (status `NO_SELECT`). If calls the function `select()`, the
form passes through the states `SELECTED` and `MOVING` and then it
returns to the `NO_SELECT` state. The corresponding state diagram is
shown below:

![img](etats.png)


# Collect the shapes

All shapes created by the user are stored in vectors:

```c++
    vector<Shape *> all_shapes;   // the vector containing all shapes
    vector<Shape *> sel_shapes;   // the vector containing only selected shapes
    Shape *moving = nullptr;      // the shape to be moved
```
    
To draw all shapes, just call their draw function:

```c++
    for (Shape *p: all_shapes) p->draw();
```
    
The vector `sel_shapes` contains pointers to the shapes that have been
selected with a mouse click. The `moving` pointer points to a form
that has received two clicks, and it is therefore in the `MOVING`
state.

All the pointers to the shapes are in `all_shapes`, so `sel_shapes` is
a subset of `all_shapes`. The pointer `moving` is never found in
`sel_shapes`.


## Question 1

Explain why the `Shape::draw()` and `Shape::is_inside()` are virtual,
and why the functions `Shape::move()` and `Shape::select()` are not?


### Answer

*Write your answer here.*


# Delete Shapes

To create a shape, you must:

-   press the 't' key to create a triangle;
-   press the 'r' key to create a rectangle;
-   press the 'c' key to create a circle.

For simplicity, the shapes are created of the same size, and are
always positioned with their center on the cursor.


## Question 2

Add the feature to delete shapes. If the user press the 'd' key, the
selected shapes are deleted. If a form is in the `MOVING` state, it is
not deleted.

Describe the policy used to delete the selected shapes.


### Answer

*Write your answer here.*



# Group shapes

It is possible to group shapes to create a *composite* shape. To do
this, we use the programming pattern **Composite**:

![img](composite.png)

The class contains a list of shapes, but it is itself a shape. To draw
a group, just draw all the forms contained in the group. To move a
group, you must move all shapes in the group, etc.

The user of our program can group shapes. First it selects all the
shapes to group, and after it presses 'g'. The program creates a new
`ShapeGroup` object and all the selected shapes are added to the
group.

To break down a group, select it and press the key 'u'. The program
does the opposite: it extracts all forms contained in the group and
deletes the group.

Finally, when one deletes a group of forms, all forms contained in the
group are deleted (the corresponding objects are destroyed).


## Question 3

Create the `ShapeGroup` class, and define its interface and operation
of its methods.

Consider the methods `Shape::is_inside()`, `Shape::move()`,
`Shape::select()`, `Shape::draw()`. Should we change the declaration
and/or definition of these methods?


### Answer

*Write your answer here.*
